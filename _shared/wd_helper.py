"""
wd_helper aka webdriver helper

webdriver wait for visible python ref. https://stackoverflow.com/a/40708217
"""

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

# gathering all often-used import from :selenium here
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys


WDW_TIMEOUT=6  # WDW_xx aka WebDriverWait_xx


def wait_by_xpath(wd:'webdriver', xpath, timeout=None):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT if timeout is None else timeout)  # wdw aka wd_wait
    e = wdw.until(EC.visibility_of_element_located((By.XPATH, xpath)))  # e aka element
    return e


def presence_all_by_css(wd:'webdriver', css, timeout=None):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT if timeout is None else timeout)  # wdw aka wd_wait
    e_all = wdw.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, css)))  # e aka element
    return e_all


def wait_invisible_by_xpath(wd:'webdriver', xpath, timeout=None):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT if timeout is None else timeout)  # wdw aka wd_wait
    e = wdw.until(EC.invisibility_of_element_located((By.XPATH, xpath)))  # e aka element
    return e


def wait_visible_by_xpath(wd:'webdriver', xpath, timeout=None):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT if timeout is None else timeout)  # wdw aka wd_wait
    e = wdw.until(EC.visibility_of_element_located((By.XPATH, xpath)))  # e aka element
    return e


def hover_to_xpath(wd: 'webdriver', xpath):
    element_to_hover_over = wait_by_xpath(wd, xpath)
    ActionChains(wd).move_to_element(element_to_hover_over).perform()


def scroll_to_xpath(wd: 'webdriver', xpath):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT)  # wdw aka wd_wait
    e = wdw.until(EC.presence_of_element_located((By.XPATH, xpath)))  # e aka element

    wd.execute_script("arguments[0].scrollIntoView()", e)  # ref. https://stackoverflow.com/a/39067206/248616

    return e


def scroll_to_element(wd: 'webdriver', e: 'element'):
    wd.execute_script("arguments[0].scrollIntoView()", e)  # ref. https://stackoverflow.com/a/39067206/248616


def wait_visible_by_css(wd:'webdriver', css, timeout=None):
    wdw = WebDriverWait(wd, timeout=WDW_TIMEOUT if timeout is None else timeout)  # wdw aka wd_wait
    e = wdw.until(EC.visibility_of_element_located((By.CSS_SELECTOR, css)))  # e aka element
    return e
