import os
from datetime import datetime
import re
import sys


pwd=os.path.abspath(os.path.dirname(__file__))
AH=pwd  # aka APP_HOME


def ts(number_only=False):  # aka timestamp
    ts = datetime.utcnow().strftime('%Y%m%d-%H%M%S') + \
         '.' + \
         datetime.utcnow().strftime('%f')[:2]  # only take 2 digit of milisecond
    if number_only:
        ts = re.sub(r'[^0-9]', '', ts)
    return ts

def sysexit(wd, errorcode=0):
    print('Exit as required')
    wd.quit  # quit() to kill the instance; otherwise, it will still run and take up machine's resource
    sys.exit(errorcode)
