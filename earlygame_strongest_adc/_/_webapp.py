import json
import os
from time import sleep

from dotenv import load_dotenv

from _shared.wd import load_webdriver_f_autoinstall_chrome_wd
from _shared.wd_helper import wait_visible_by_xpath, TimeoutException
from _shared.common import ts, sysexit

CH=os.path.abspath(os.path.dirname(__file__))  # CH aka CODEHOME

#region load envvar
load_dotenv(dotenv_path=f'{CH}/.env', override=False)
USE_CACHE = os.environ.get('USE_CACHE')
#endregion load envvar


#region helper

def ss(t):
    r=wd.save_screenshot(f'{CH}/s/{ts()}--{t}.png'); assert r


def to_jsonfile(array, save_to):
    s = json.dumps(array, indent=2)
    with open(save_to, 'w') as f:
        f.write(s)

CHAMPION_LIST_FILE=f'{CH}/io/champion_l.json'


def get_champion_list(wd):
    url = 'https://leagueoflegends.fandom.com/wiki/List_of_champions'
    wd.get(url)

    wait_visible_by_xpath(wd, '//h2/span[text()="List of Available Champions"]') ; ss('champion-list')

    e_champion_list = wd.find_elements_by_css_selector('.article-table td[data-sort-value] .champion-icon a')
    champion_l = []
    for e_champion in e_champion_list:
        e = e_champion

        link            = e.get_attribute('href'),
        name, titlename = e.text.split('\n')

        c = {
            'link'      : link,
            'name'      : name,
            'titlename' : titlename.title(),
        }
        champion_l.append(c)
    to_jsonfile(champion_l, save_to=CHAMPION_LIST_FILE)
    return champion_l


def fromcache_champion_list():
    with open(CHAMPION_LIST_FILE) as f:
        s = f.read()
        j = json.loads(s)
    return j

#endregion helper


wd = load_webdriver_f_autoinstall_chrome_wd()  # wd aka webdriver

try:
    '''
    sample :wd command
        wait_visible_by_xpath(wd, "//*[text()='abb']")
        e.send_keys(email) ; ss('abb--0-ccc')
        e.click()
        sleep(6)
    '''

    if USE_CACHE=='1': champion_l = fromcache_champion_list()
    else:              champion_l = get_champion_list(wd)

    print(json.dumps(champion_l, indent=2))

except TimeoutException:
    print('!!! Time out')
    sysexit(wd, 1)
finally:
    wd.quit()
