#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
    SR=$(cd "$SH/.." && pwd)  # SR aka SOURCE_ROOT
    AH=$(cd "$SH/_" && pwd)      # AH aka APPHOME

        echo $SR; echo
        cd $SR
            PYTHONPATH=`pwd`  pipenv run  python "$AH/_webapp.py"
